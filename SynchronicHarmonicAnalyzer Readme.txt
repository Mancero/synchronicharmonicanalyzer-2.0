SynchronicHarmonicAnalyzer Readme.txt

Dispositif d'analyse harmonique et représentation des champs acoustiques, dans le domaine des fréquences.

Développé par Daniel Mancero Baquerizo, CICM - Université Paris 8 ©2019

www.danielmancero.com

